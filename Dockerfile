FROM python:3.9-alpine

ENV PYTHONUNBUFFERED=1

COPY ./requirements.txt /requirements.txt

# Установка клиента PostgreSQL
RUN apk add --update --no-cache postgresql-client

# Установка отдельных зависимостей,
# чтобы избежать установки лишних пакетов
RUN apk add --update --no-cache --virtual .tmp-build-deps \
    gcc libc-dev linux-headers postgresql-dev
RUN pip install -r /requirements.txt

# Удаление зависимостей
RUN apk del .tmp-build-deps

RUN mkdir /app
WORKDIR /app
COPY ./app /app

# [Безопасность] Ограничение прав пользователя, который будет запускать образ
RUN adduser -D user

USER user

# Добавляем CMD с командой для запуска Django приложения
# Предполагается, что manage.py находится в корне каталога приложения
# и приложение будет слушать на всех интерфейсах (0.0.0.0) на порту 8000
CMD ["python", "manage.py", "runserver", "0.0.0.0:3003"]